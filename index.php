<?php
   /**
   * Framajauge
   * Copyright (C) 2014 Framajauge team
   *
   * This program is free software; you can redistribute it and/or
   * modify it under the terms of the GNU General Public License
   * as published by the Free Software Foundation; either version 2
   * of the License, or (at your option) any later version.
   *
   * This program is distributed in the hope that it will be useful,
   * but WITHOUT ANY WARRANTY; without even the implied warranty of
   * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   * GNU General Public License for more details.
   *
   * See /LICENCE for more information
   * @contact rbataille@april.org
   */

   $script_hash = md5(file_get_contents("./js/framajauge.js"));
   $jauge_js_link = "./js/framajauge.js?v=$script_hash";
?>
<html>
<head>
  <script type="text/javascript" src="./js/jquery-2.1.1.js"></script>
  <script type="text/javascript" src="<?php echo $jauge_js_link; ?>"></script>
  <title>Framajauge</title>
</head>
<body>
  <select id="template_selector">
    <option value="001">001</option>
    <option value="002">002</option>
  </select>
</body>
</html>
