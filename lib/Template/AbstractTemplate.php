<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */

abstract class AbstractTemplate{
    protected $name; // current template file name
    protected $content; // current template content
    protected $hash; // current template md5 hash
    protected $version;
    protected $values;
  
    abstract public function __construct($__values);
    abstract public function load($__name);	
  
    /**
     * echo template content, set http header to text/json 
     */
    public function content(){
        $raw_content = $this->minify_content($this->content);
        $this->hash = md5($raw_content);
        $final_form = $this->content_final_form($raw_content);
	
        header("Content-Type: application/json");
        echo $this->minify_content($final_form);
    }

    /**
     *  Minify text in param
     */
    public function minify_content($__content){
        $raw_content = str_replace("\t", "", $__content);
        $raw_content = str_replace("\r", "", $raw_content);
        $raw_content = str_replace("\n", "", $raw_content);
	
        $raw_content = $this->remove_useless_space($raw_content);
        return $this->replace_needed($raw_content);
    }

    /**
     *  Replace all %%key%% by it's true value
     */
    private function replace_needed($__content){
        foreach($this->values as $key => $value){
            $__content = str_replace("%%".$key."%%", $value, $__content);
        }
        return $__content; 
    }

    /**
     *  Remove useless blank space for minification
     */
    private function remove_useless_space($__content){
        $in_block = false;
        $final_content = "";
        for($index = 0; $index < strlen($__content); ++$index){
            if($__content[$index] == "\""){
                $in_block = !$in_block;
            }
            if($__content[$index] === " "){
                if($in_block){
                    $final_content .= $__content[$index];
                }
            }
            else{
                $final_content .= $__content[$index];
            }
        }
        return $final_content;
    }

    /**
     *  add some data to final content
     */
    private function content_final_form($__raw_content){
        return '{"version":"'.$this->version.'",
                 "hash":"'.$this->hash.'",
                 "jauge":'.$__raw_content.'}';
    }     

}
