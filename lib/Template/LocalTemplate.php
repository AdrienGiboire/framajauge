<?php
/**
 * Framajauge
 * Copyright (C) 2014 Framajauge team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See /LICENCE for more information
 * @contact framajauge@framalistes.com
 */

require_once dirname(__FILE__) . DS . "AbstractTemplate.php";
/** 
 * Template class, load needed JSON, minify it and send it to browser
 * (replace "%%key%%" based string by it's real content)
 */
class LocalTemplate extends AbstractTemplate{
    /**
     * Construct default template
     */
    public function __construct($__values){
        $this->values = $__values; 
        $template_id = (isset($_POST["template_id"])?$_POST["template_id"]:"001");
        $this->load($template_id);
    }

    /**
     * Load template by file name
     */
    public function load($__name){
        $this->version = "1.0";
        $this->name = dirname(__FILE__) .  DS . "../../templates/".$__name.".json";
        $this->content = file_get_contents($this->name);	
    }	

}
